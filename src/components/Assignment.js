import React, { Component } from "react";
import SearchBar from "./SearchBar";
import FileContent from "./FileContent";
import 'materialize-css/dist/css/materialize.min.css';
import axios from 'axios';

 
class Assignment extends Component {

    dirUrl = "http://localhost:8080/file/list?dir=";
    fileUrl = "http://localhost:8080/file/detail?file=";

    constructor(props) {
        super(props);
        this.state = {
            response: "response ...",
            searchText: "",
            fileType: "dir"
        };
    }

    render() {
        return (
            <div className="container">
                <SearchBar 
                  onGetClick={() => this.getData()}
                  onSearchTextChange={(v) => this.searchTextChange(v)}
                  onFileTypeChange={(v) => this.fileTypeChange(v)} />
                <FileContent value={this.state.response}/>
            </div>
        )
    }

    searchTextChange(value) {
        this.setState( { searchText: value} );
    }

    fileTypeChange(value) {
        this.setState( { fileType: value} );
    }

    getData() {
        this.setState( { response: "" } );
        const url = this.state.fileType === "dir" ? 
                        this.dirUrl + this.state.searchText:
                        this.fileUrl + this.state.searchText;
        axios.get(url)
            .then(response => {
                this.handleResponse(JSON.stringify(response.data));
            })
            .catch(error => {
                if (error.message === 'Network Error') {
                    console.log(error.message);
                    this.handleResponse("cannot connect to server");
                } else {
                    console.log(error.response.data);
                    this.handleResponse(JSON.stringify(error.response.data));
                }
            });
    }

    handleResponse(resp) {
        this.setState( { response: resp } );
    }
}

export default Assignment;
