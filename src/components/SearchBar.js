import React, { Component } from "react";

 
class SearchBar extends Component {

    getClick = () => {
        this.props.onGetClick();
    }

    searchTextChange = (event) => {
        this.props.onSearchTextChange(event.target.value);
    }

    fileTypeChange = (event) => {
        this.props.onFileTypeChange(event.target.id);
    }

    render() {
        return (
            <div className="row">
                <div className="input-field col s12">
                    <input id="search_box" type="text" className="validate" value={this.props.searchText}
                      onChange={this.searchTextChange} />
                    <label htmlFor="search_box"></label>
                </div>
                <div className="col s6">
                    <label>
                        <input id="dir" name="group1" type="radio" onClick={this.fileTypeChange} />
                        <span>Dir</span>
                    </label>
                    <label>
                        <input id="file" name="group1" type="radio" onClick={this.fileTypeChange} />
                        <span>File</span>
                    </label>
                </div>
                <div className="col s6">
                    <a className="waves-effect waves-light btn" onClick={this.getClick}>get</a>
                </div>
            </div>
        )
    }

}

export default SearchBar;
